package apkbuild

import (
	"context"
	"io"
	"log"
	"strings"

	"mvdan.cc/sh/v3/expand"
	"mvdan.cc/sh/v3/interp"
	"mvdan.cc/sh/v3/syntax"
)

// An APKBUILD file together with a package name used for reporting
type ApkbuildFile struct {
	PackageName string
	Content     io.Reader
}

func NewApkbuildFile(pkgname string, content io.Reader) ApkbuildFile {
	return ApkbuildFile{pkgname, content}
}

// Parse the given file and return an Apkbuild struct with the parsed
// information. This is done by interpreting the code and then extracting
// variables from the interpreter, like if the APKBUILD file was sourced in a
// shell.
//
// The interpreter does not execute any external commands (no fork/exec), but
// logs a warning in case a non built-in command is encountered. Subshells are
// supported (as long as they don't invoke external commands).
//
// Environment variables can be provided through the expand.Environ interface.
// Use expand.ListEnviron(os.Environ()...) to use the system's environment
// variables.
func Parse(file ApkbuildFile, env expand.Environ) (apkbuild Apkbuild, err error) {
	parser := syntax.NewParser()
	parsedFile, err := parser.Parse(file.Content, file.PackageName)

	if err != nil {
		return
	}

	runner, err := interp.New(
		interp.Env(env),
		interp.ExecHandlers(func(next interp.ExecHandlerFunc) interp.ExecHandlerFunc {
			return func(ctx context.Context, args []string) error {
				log.Printf("Warning: package %s tries to execute '%s'", file.PackageName, strings.Join(args, " "))
				return next(ctx, args)
			}
		}),
	)

	if err != nil {
		return
	}

	ctx := context.Background()
	err = runner.Run(ctx, parsedFile)
	if err != nil {
		return
	}

	apkbuild.Pkgname = runner.Vars["pkgname"].Str
	apkbuild.Pkgver = runner.Vars["pkgver"].Str
	apkbuild.Pkgrel = runner.Vars["pkgrel"].Str
	apkbuild.Pkgdesc = runner.Vars["pkgdesc"].Str
	apkbuild.Url = runner.Vars["url"].Str
	apkbuild.License = runner.Vars["license"].Str
	apkbuild.Options = parseOptions(runner.Vars["options"].Str)
	apkbuild.Arch = NewArchesFromString(runner.Vars["arch"].Str)

	apkbuild.Depends = parsePackageList(runner.Vars["depends"].Str)
	apkbuild.DependsDev = parsePackageList(runner.Vars["depends_dev"].Str)

	apkbuild.Makedepends = parsePackageList(runner.Vars["makedepends"].Str)
	apkbuild.MakedependsBuild = parsePackageList(runner.Vars["makedepends_build"].Str)
	apkbuild.MakedependsHost = parsePackageList(runner.Vars["makedepends_host"].Str)

	apkbuild.Checkdepends = parsePackageList(runner.Vars["checkdepends"].Str)

	apkbuild.Install = strings.Fields(runner.Vars["install"].Str)
	apkbuild.Pkggroups = strings.Fields(runner.Vars["pkggroups"].Str)
	apkbuild.Pkgusers = strings.Fields(runner.Vars["pkgusers"].Str)

	apkbuild.Triggers = NewTriggersFromString(runner.Vars["triggers"].Str)

	apkbuild.Source = parseSource(runner.Vars["source"].Str)

	apkbuild.Provides = parsePackageList(runner.Vars["provides"].Str)
	apkbuild.ProviderPriority = runner.Vars["provider_priority"].Str
	apkbuild.Replaces = parsePackageList(runner.Vars["replaces"].Str)

	apkbuild.Subpackages = parseSubpackage(runner.Vars["subpackages"].Str)

	apkbuild.Builddir = runner.Vars["builddir"].Str

	apkbuild.Sha512sums = parseSourceHash(runner.Vars["sha512sums"].Str)

	apkbuild.Funcs = runner.Funcs

	return
}

func parseOptions(options string) (parsedOptions Options) {
	return Options(strings.Fields(options))
}

func parsePackageList(packageList string) (packages PackageSpecs) {
	for _, pkg := range strings.Fields(packageList) {
		pkgspec := NewPkgSpecFromString(pkg)
		packages = append(packages, pkgspec)
	}

	return
}

func parseSource(sourceString string) (sources []Source) {
	for _, sourceItem := range strings.Fields(sourceString) {
		source := Source{}
		if strings.Contains(sourceItem, "::") {
			parts := strings.Split(sourceItem, "::")
			source.Filename = parts[0]
			source.Location = parts[1]
		} else {
			source.Location = sourceItem
			source.Filename = sourceItem[strings.LastIndex(sourceItem, "/")+1:]
		}
		sources = append(sources, source)
	}

	return
}

func parseSubpackage(subpackageString string) (subpackages []Subpackage) {
	const (
		IdxPkgName = iota
		IdxSplitFunc
		IdxArch
	)
	for _, subpackageItem := range strings.Fields(subpackageString) {
		components := strings.Split(subpackageItem, ":")
		subpackage := Subpackage{}

		nr := len(components)
		switch {
		case nr >= 3:
			subpackage.Arch = components[IdxArch]
			fallthrough
		case nr == 2:
			subpackage.SplitFunc = components[IdxSplitFunc]
			fallthrough
		case nr == 1:
			subpackage.Subpkgname = components[IdxPkgName]

		}
		subpackages = append(subpackages, subpackage)
	}
	return
}

func parseSourceHash(sums string) (sourceHashes []SourceHash) {
	sourceHashItems := strings.Fields(sums)
	for i := 0; i < len(sourceHashItems); i += 2 {
		source := ""
		if i+1 < len(sourceHashItems) {
			source = sourceHashItems[i+1]
		}

		sourceHash := SourceHash{
			Hash:   sourceHashItems[i],
			Source: source,
		}
		sourceHashes = append(sourceHashes, sourceHash)
	}

	return
}
