package version

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

var validVersions = [][]string{
	{"1.2.3_git12341233", "GitSuffix"},
	{"0.01", "LeadingZero"},
	{"0", "SingleNumber"},
	{"0.0", "TwoComponents"},
	{"0.0.0", "ThreeComponents"},
	{"0.0.0.0", "FourComponents"},
	{"1.02.3", "LeadingZero"},
	{"1.2.345123", "LongerComponent"},
	{"1.2.3_hg1212321", "HGSuffix"},
	{"1.2.3_svn1231231", "SVNSuffix"},
	{"1.2.3_p4", "PatchSuffix"},
	{"1.2.3a", "SuffiXLetter"},
	{"1.2.3c1", "SuffixLetterWithNumber"},
	{"1.2.3_rc1", "RCSuffixWithNumber"},
	{"1.2.3_alpha", "SuffixWithoutNumber"},
	{"1.2.3_alpha3", "AlphaSuffixWithNumber"},
	{"1.2.3_beta5", "BetaSuffixWithNumber"},
	{"1.2.3_pre2", "PreSuffixWithNumber"},
	{"a", "SingleLetter"},
	{"_pre0", "StartWithSuffix"},
}

var invalidVersions = [][]string{
	{"ab", "TwoConsecutiveLetters"},
	{"1git", "SufixWithoutUnderscore"},
	{"1_2", "SuffixWithoutType"},
	{"1-2", "RevisionWithoutLetter"},
	{"1_a", "LetterWithUnderscoreSeparator"},
	{"1__pre", "DoubleUnderscore"},
	{"1_pre_3", "UnderscoreSuffixSeparator"},
	{"1_pre.3", "SuffixInBetween"},
	{"123123123123123122", "SingleNumericComponentTooLong"},
	{"1.2a.3", "LetterInBetween"},
}

func TestIsValidWithValidCases(t *testing.T) {
	for _, subtest := range validVersions {
		t.Run(subtest[1], func(t *testing.T) {
			require.True(t, IsValidWithoutRevision(subtest[0]), fmt.Sprintf("'%s' should be a valid version", subtest[0]))
		})
	}
}

func TestIsValidWithInvalidCases(t *testing.T) {
	for _, subtest := range invalidVersions {
		t.Run(subtest[1], func(t *testing.T) {
			require.False(t, IsValidWithoutRevision(subtest[0]), fmt.Sprintf("'%s' is not a valid version", subtest[0]))
		})
	}
}

func ExampleIsValidWithoutRevision_valid() {
	fmt.Println(IsValidWithoutRevision("1.2.4_git123"))
	// Output: true
}

func ExampleIsValidWithoutRevision_invalid() {
	fmt.Println(IsValidWithoutRevision("1.2.4_5"))
	// Output: false
}
