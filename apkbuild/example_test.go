//go:build unit || !integration

package apkbuild_test

import (
	"fmt"
	"strings"

	"github.com/MakeNowJust/heredoc/v2"
	"gitlab.alpinelinux.org/alpine/go/apkbuild"
	"mvdan.cc/sh/v3/expand"
)

func ExampleParse() {
	pkgContents := strings.NewReader(heredoc.Doc(`
		pkgname="example-package"
		pkgver="1.2.0"
	`))

	apkbuildFile := apkbuild.ApkbuildFile{
		PackageName: "example-package",
		Content:     pkgContents,
	}

	pkg, err := apkbuild.Parse(apkbuildFile, expand.ListEnviron())

	if err != nil {
		panic(err)
	}

	fmt.Println(pkg.Pkgver)
	// Output: 1.2.0
}
