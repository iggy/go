module gitlab.alpinelinux.org/alpine/go

go 1.21

toolchain go1.21.6

require (
	github.com/MakeNowJust/heredoc/v2 v2.0.1
	github.com/stretchr/testify v1.9.0
	golang.org/x/exp v0.0.0-20240707233637-46b078467d37
	gopkg.in/ini.v1 v1.67.0
	gopkg.in/yaml.v3 v3.0.1
	mvdan.cc/sh/v3 v3.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/term v0.17.0 // indirect
)
