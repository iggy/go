package apkbuild

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewPkgSpecFromStringParsesPkgName(t *testing.T) {
	pkgspec := NewPkgSpecFromString("my-package")

	assert.Equal(t, "my-package", pkgspec.Pkgname)
	assert.False(t, pkgspec.Conflicts)
	assert.Empty(t, pkgspec.Constraint)
	assert.Empty(t, pkgspec.Version)
	assert.Empty(t, pkgspec.RepoPin)
}

func TestNewPkgSpecFromStringParsesConflictingPackage(t *testing.T) {
	pkgspec := NewPkgSpecFromString("!my-package")

	assert.Equal(t, "my-package", pkgspec.Pkgname)
	assert.True(t, pkgspec.Conflicts)
}

func TestNewPkgSpecFromStringParsesLessThanConstraint(t *testing.T) {
	pkgspec := NewPkgSpecFromString("my-package<2.0")
	assert.Equal(t, "my-package", pkgspec.Pkgname, "pkgspec.Pkgname")
	assert.Equal(t, "<", pkgspec.Constraint, "pkgspec.Constraint")
	assert.Equal(t, "2.0", pkgspec.Version, "pkgspec.Version")
}

func TestNewPkgSpecFromStringParsesLessThanOrEqualsConstraint(t *testing.T) {
	pkgspec := NewPkgSpecFromString("my-package<=2.0")
	assert.Equal(t, "my-package", pkgspec.Pkgname, "pkgspec.Pkgname")
	assert.Equal(t, "<=", pkgspec.Constraint, "pkgspec.Constraint")
	assert.Equal(t, "2.0", pkgspec.Version, "pkgspec.Version")
}

func TestNewPkgSpecFromStringParsesFuzzyOperator(t *testing.T) {
	pkgspec := NewPkgSpecFromString("my-package~2.0")
	assert.Equal(t, "my-package", pkgspec.Pkgname, "pkgspec.Pkgname")
	assert.Equal(t, "~", pkgspec.Constraint, "pkgspec.Constraint")
	assert.Equal(t, "2.0", pkgspec.Version, "pkgspec.Version")
}

func TestNewPkgSpecFromStringParsesFuzzyEqualsOperator(t *testing.T) {
	pkgspec := NewPkgSpecFromString("my-package~=2.0")
	assert.Equal(t, "my-package", pkgspec.Pkgname, "pkgspec.Pkgname")
	assert.Equal(t, "~=", pkgspec.Constraint, "pkgspec.Constraint")
	assert.Equal(t, "2.0", pkgspec.Version, "pkgspec.Version")
}

func TestNewPkgSpecFromStringParsesRepositoryPins(t *testing.T) {
	pkgspec := NewPkgSpecFromString("my-package@community")
	assert.Equal(t, "my-package", pkgspec.Pkgname, "pkgspec.Pkgname")
	assert.Equal(t, "community", pkgspec.RepoPin, "pkgspec.RepoPin")
}

func TestNewPkgSpecFromStringParsesFullSpec(t *testing.T) {
	pkgspec := NewPkgSpecFromString("!my-package<=3.0.0-r1@community")
	assert.Equal(t, "my-package", pkgspec.Pkgname, "pkgspec.Pkgname")
	assert.True(t, pkgspec.Conflicts, "pkgspec.Conflicts")
	assert.Equal(t, pkgspec.Constraint, "<=", "pkgspec.Constraint")
	assert.Equal(t, pkgspec.Version, "3.0.0-r1", "pkgspec.Version")
	assert.Equal(t, "community", pkgspec.RepoPin, "pkgspec.RepoPin")
}

func TestNewPkgSpecFromStringParsesPkgNameWithAllLetters(t *testing.T) {
	pkgname := "abcdefghijklmnopqrstuvwxyz-0123456789"
	pkgspec := NewPkgSpecFromString(pkgname)

	assert.Equal(t, pkgname, pkgspec.Pkgname)
	assert.False(t, pkgspec.Conflicts)
	assert.Empty(t, pkgspec.Constraint)
	assert.Empty(t, pkgspec.Version)
	assert.Empty(t, pkgspec.RepoPin)
}

func TestArchEnabledAll(t *testing.T) {
	arches := NewArchesFromString("all")
	assert.True(t, arches.Enabled("x86_64"))
	assert.True(t, arches.Enabled("x86"))
}

func TestArchEnabledAllWithDisabledArches(t *testing.T) {
	arches := NewArchesFromString("all !x86")
	assert.True(t, arches.Enabled("x86_64"))
	assert.False(t, arches.Enabled("x86"))
}

func TestArchEnabledNoarch(t *testing.T) {
	arches := NewArchesFromString("noarch")
	assert.True(t, arches.Enabled("x86_64"))
}

func TestArchEnabledWithSpecifiedArches(t *testing.T) {
	arches := NewArchesFromString("x86_64 x86")

	assert.True(t, arches.Enabled("x86_64"))
	assert.True(t, arches.Enabled("x86"))
	assert.False(t, arches.Enabled("armv7"))
}

func TestNewTriggerFromString(t *testing.T) {
	assert := assert.New(t)

	trigger := NewTriggerFromString("foo.trigger=/usr/share/foo/*:/var/lib/foo/*")
	assert.Equal("foo.trigger", trigger.Scriptname)
	require.Len(t, trigger.Paths, 2)
	assert.Equal("/usr/share/foo/*", trigger.Paths[0])
	assert.Equal("/var/lib/foo/*", trigger.Paths[1])
}

func TestSourceIsRemote(t *testing.T) {
	source := Source{
		Filename: "test.tar.gz",
		Location: "https://example.com/test.tar.gz",
	}
	assert.True(t, source.IsRemote())

	source.Location = "http://example.com/test.tar.gz"
	assert.True(t, source.IsRemote())

	source.Location = "ftp://example.com/test.tar.gz"
	assert.True(t, source.IsRemote())

	source.Location = "test.tar.gz"
	assert.False(t, source.IsRemote())

	source.Location = "fix-build.patch"
	assert.False(t, source.IsRemote())

	source.Location = "patches/fix-build.patch"
	assert.False(t, source.IsRemote())
}
