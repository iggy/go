package version

import (
	"bytes"
	"slices"
	"unicode"
)

var validStateTransitions = map[Token][]Token{
	Digit:       {Digit, DigitOrZero, Letter, Suffix, End},
	DigitOrZero: {DigitOrZero, Letter, Suffix, End},
	Suffix:      {SuffixDigit, End},
	SuffixDigit: {SuffixDigit, End},
	Letter:      {Digit, Suffix, End},
}

// IsValidWithoutRevision validates the provided version without a revision component.
func IsValidWithoutRevision(version string) bool {
	v := Version(version)
	nv := &v
	t := Digit
	var pt Token
	componentLength := 0

	for t != End {
		pt = t
		t, nv = nv.NextToken(t)
		tokenRune := []rune(*nv)

		switch t {
		case Digit:
			// '-' is considered to be a digit, but only valid when part of the revision
			if (*nv)[0] == '-' {
				return false
			}
			componentLength++
		case Suffix:
			var suffix bytes.Buffer
			for _, r := range tokenRune {
				if unicode.IsLower(r) {
					suffix.WriteRune(r)
				} else {
					pt, t = Suffix, SuffixDigit
					break
				}
			}
			ov := Version(tokenRune[suffix.Len():])
			nv = &ov
			if !isValidSuffix(suffix.String()) {
				return false
			}
		}

		if !slices.Contains(validStateTransitions[pt], t) {
			return false
		}

		if pt == Digit && t != Digit {
			if componentLength >= 18 {
				return false
			}
			componentLength = 0
		}

		if len(*nv) > 0 &&
			t != DigitOrZero &&
			t != RevisionDigit &&
			!(pt == Suffix && t == SuffixDigit) {

			a := Version((*nv)[1:])
			nv = &a
		}
	}

	return true
}

func isValidSuffix(suffix string) bool {
	if len(suffix) == 0 {
		return false
	}
	if !slices.Contains(preSuffixes, suffix) && !slices.Contains(postSuffixes, suffix) {
		return false
	}

	return true
}
