// Package version tokenizes a apk package version using the same algorithm as
// apk-tools uses.
//
// A valid version can be described by the following EBNF specificion:
//
//	version     = digits , { "." , digits } , [ letter, digits ] , { suffix } , [ revision ] ;
//
//	digit       = "0" | "1" | "2" | "3"| "4" | "5" | "6" | "7" | "8" | "9" ;
//	digits      = { digit } ;
//	revision    = "-r", digit, digits ;
//
//	letter      = "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" | "m"
//	            | "n" | "o" | "p" | "q" | "r" | "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z" ;
//
//	suffix      = "_" , ( pre-suffix | post-suffix ) , digits ;
//
//	pre-suffix  = "alpha" | "beta" | "pre" | "rc" ;
//	post-suffix = "cvc" | "svn" | "git" | "hg" | "p" ;
package version
